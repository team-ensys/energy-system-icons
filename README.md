# Energy System Icons

![](png/house.png) ![](png/pv.png) ![](png/battery.png) ![](png/heatpump.png) ![](png/boiler.png) ![](png/ecar.png) ![](png/charging_station.png) ![](png/wind_power.png) ![](png/grid.png) 
![](png/smart_meter.png) ![](png/smartphone.png) ![](png/computer.png) ![](png/flexibility_optimization.png) ![](png/cloud.png)

A collection of vector graphic icons for the illustration of energy systems and flexibility options designed by the [TEAM-EnSys working group](https://eeg.tuwien.ac.at/staff/organization#group-6) at the [Energy Economics Group (EEG)](https://eeg.tuwien.ac.at) at [TU Wien](https://www.tuwien.at/).

If you use these icons in your work, please provide the following reference:
```
The icons were designed by TU Wien ESEA (Source: https://gitlab.com/team-ensys/energy-system-icons)
```

All icons were created using [inkscape](https://inkscape.org/).
